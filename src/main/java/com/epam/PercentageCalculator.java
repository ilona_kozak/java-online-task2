package com.epam;

public class PercentageCalculator {
    public double obtained;
    public double total;

    public double calculatePercentage() {
        return obtained * 100 / total;
    }
}
