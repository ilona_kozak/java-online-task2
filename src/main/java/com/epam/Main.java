package com.epam;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Main {
    public static Scanner scanner = new Scanner(System.in);
    public static void main(String[] args) {
        Map<Integer, Guest> guests = new HashMap<Integer, Guest>();
        System.out.println("Enter, how many guests were invited: ");
        int n = scanner.nextInt();
        for (int i = 1; i <= n; i++) {
            System.out.println("Guest's name is ");
            String name = scanner.next();  // please, always enter Alice for the first place and Bob for the second one
            guests.put(i, new Guest(name));
        }
        for (Map.Entry<Integer, Guest> people : guests.entrySet()) {
            System.out.printf("Key: %s  Name: %s \n", people.getKey(), people.getValue().getName());
        }

        int a = 1;
        int b = n;
        Map<Integer, Guest> informedPeople = new HashMap<Integer, Guest>();
        for (int i = 0; i < n; i++){
            int random = a + (int) (Math.random() * b);
            informedPeople.put(1, new Guest("Alice"));
            informedPeople.put(2, new Guest("Bob"));
            if (informedPeople.containsKey(random) && random != 1) {  //informedPeople.containsKey(random) -- you can even repeating name of guests
                System.out.print("\nThis guy heard about the rumor... (" + guests.get(random).getName() + ")");
                break;
            } else if (random == 1) {
                System.out.print("\nTsss, this's Alice... Run away!");
            } else {
                System.out.print("\n" + guests.get(random).getName() + ", ");
                Guest.tellingRumor();
            }
            informedPeople.put(random, new Guest(guests.get(random).getName()));
        }

        PercentageCalculator pc = new PercentageCalculator();
        pc.obtained = informedPeople.size() - 1; // excepting Alice
        pc.total = guests.size() - 1; // excepting Alice

        System.out.println("\nPercentage obtained: " + (pc.calculatePercentage())); // how many guys hear about the rumor, excepting Alice
    }
}
